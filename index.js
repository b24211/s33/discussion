// JavaScript Synchronous vs Asynchronous
// Synchronous - meaning only one statement is executed at a time
// Asynchronous - means that we can proceed to execute other statements while time consuming code is running in the background

console.log("Hello");
// conosle.log("Hello again");

// Getting all posts
/*
	SYNTAX:
		fetch('URL');
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Checking the status of the request
/*
	fetch('URL')
	.then((response) => {})
*/
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// Retrieve the contents/data from the "Response" object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));

// Creates a function using "async" and "await" keywords.

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returns a promise
	console.log(result);
	// Returns the type of the response
	console.log(typeof result);
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	console.log(json);
}

fetchData();

// Retrieve a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// Create a post
fetch('https://jsonplaceholder.typicode.com/posts',{
	method: 'POST',
	headers: {
		'Content-Type': 'application.json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a post using the PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello again!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a post using the PATCH method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
})

// Filtering posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));

// Retrieve comments of a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));